Selección de Historias de Usuario
=================================

Se escogió esta historia de usuario debido a que el administrador del edificio es el encargado de que todos los turnos tengan que estar suplidos por algún funcionario, por lo que es relevante que maneje la información de los reemplazantes con el fin de facilitar las solicitudes de reemplazo cuando se necesite.


Control de Turnos (OB-1)
------------------------

Se escogió esta historia de usuario debido a que el administrador del edificio es el encargado de que todos los turnos tengan que estar suplidos por algún funcionario, por lo que es relevante que maneje la información de los reemplazantes con el fin de facilitar las solicitudes de reemplazo cuando se necesite.

.. csv-table:: Historias Criticas OB-1
   :header: "Código", "Como", "Quiero", "Para"
   :widths: 15, 10, 30,30

    HU-101,Conserje,Conocer los turnos designados,trabajar
    HU-102,Administrador,Asignar turnos,Establecer calendarios de trabaj
    HU-109,Administrador,Obtener un listado de personas que trabajan como reemplazos,Administrar las solicitudes de reemplazo    

Justificación HU-101
^^^^^^^^^^^^^^^^^^^^

Ya que los conserjes son uno de los principales usuarios del sistema, asi como los más interesados en conocer sus turnos de trabajo era imperante que el sistema proporcionara una forma de que estos puedan revisar sus turnos asignados, de manera que estos posean la información relevante a sus trabajos.

Justificación HU-102
^^^^^^^^^^^^^^^^^^^^
 
Al estar relacionada con la historia de usuario anterior, es absolutamente necesaria de implementar ya que sin la acción detallada en esta historia de usuario no es posible la historia HU-101. Por otra parte el administrador es el encargado de asignar los turnos, por lo cual es necesario que este calendario de turnos pueda ser ingresado al sistema por algún medio.



Justificación HU-109
^^^^^^^^^^^^^^^^^^^^

Se escogió esta historia de usuario debido a que el administrador del edificio es el encargado de que todos los turnos tengan que estar suplidos por algún funcionario, por lo que es relevante que maneje la información de los reemplazantes con el fin de facilitar las solicitudes de reemplazo cuando se necesite.

El detalle del trabajo realizado por este equipo se encuentra disponible en el siguiente `enlace  <https://drive.google.com/file/d/0B9hHRpdvfs9Kd3ZIbVM0bXpVSzg/view?usp=sharing>`_.


Uso de Espacios Comunes (OB-2)
------------------------------

El equipo encargado de la selección de las historias de usuario criticas para el objetivo dos, realizó un análisis de las historias de usuario, priorizando estas y seleccionando aquellas que consideraron partes fundamentales del nucleo del sistema. De esta manera las historias seleccionadas son:

.. csv-table:: Historias Criticas OB-2
   :header: "Código", "Como", "Quiero", "Para"
   :widths: 15, 10, 30,30

    HU-201,Arrendatario/propietario,Registrar el uso de un espacio común,Reservarlo
    HU-207,Administrador,Modificar las reservaciones de espacios comunes,Reagendarlas o eliminarlas en caso de cualquier inconveniente
    HU-212,Administrador,Registrar  o modificar espacios comunes,Que los residentes puedan arrendarlos

Justificación HU-201
^^^^^^^^^^^^^^^^^^^^

Esta historia de usuario es absolutamente necesaria ya que esta corresponde a las funcionalidad principal a la cual apunta el OB-2, es considerado a una actividad esencial para responder de manera satisfactoria al objetivo general.

Justificación HU-207
^^^^^^^^^^^^^^^^^^^^

Estrechamente relacionada con las historia anterior, esta es requerida ya que en caso de eventualidades es el administrador quien debe resolverlas por lo cual es necesario que este tenga un control sobre las reservaciones, pueda manipular y usar esta información de manera de solucionar los problemas que se le presenten.

Justificación HU-212
^^^^^^^^^^^^^^^^^^^^

Esta historia de igual manera forma parte del núcleo de la aplicación, corresponde a la acción que da origen a las otras historias de usuario, se le asignado al administrador de manera que sea este quien tenga el control de los horarios arrendables de los espacios comunes.

El detalle del trabajo realizado por este equipo se encuentra disponible en el siguiente `enlace  <https://drive.google.com/file/d/0B9hHRpdvfs9KWHFJM1JQRjBNa0U/view?usp=sharing>`_.

Mantención de Equipamientos (OB-3)
----------------------------------

Por su parte el equipo al cual se le asigno el OB-3 planteó una metodologia combinada en primer lugar estimo en base a una escala diseñada para la situacion el grado de relevancia de las distintas historias de usuario, para luego contrastar esta información con diagrama de dependencias de las historias de usuario.

.. csv-table:: Escala de Importancia
   :header: "Valoración","Detalle"
   :widths: 20,40

    1, "Crítica. Corresponde a una historia de usuario de alta prioridad o esencial para el sistema."
    2, "Necesaria. Corresponde a una historia de usuario cuya implementación en el sistema es deseada."
    3, "Opcional. Corresponde a una historia de usuario opcional, cuya implementación no es esencial para el sistema en sí."

La idea de esta escala es evaluar por grado de relevancia las distintas historias de usuario.

Por su parte el diagrama de dependencias constituye una herramienta gráfica para visualizar  las relaciones entre las distintas historias de usuario, pues permite visualizar las dependencias entre historias de usuario. 

Ya con la escala evaluada y el diagrama elaborado se podrá visualizar de manera sencilla las historias de usuario primordiales.  

.. image:: userstories/2.png

De esta manera se concluyó que las historias de usuario criticas corresponden a las siguientes:

.. csv-table:: Historias Criticas OB-3
   :header: "Código", "Como", "Quiero", "Para"
   :widths: 15, 10, 30,30

    HU-302,Administrador,Planificar los mantenimientos de los distintos equipos mes a mes,Establecer un calendario de mantenimientos
    HU-305,Conserje,Dar aviso de las mantenciones realizadas,Facilitar al administrador la planificación del calendario de mantenciones
    HU-309,Conserje,"Notificar al administrador de las tareas de mantenimiento no realizadas, junto con el motivo por el cual no fueron realizadas",Que el administrador tome las medidas correspondientes

El detalle del trabajo realizado por este equipo se encuentra disponible en el siguiente `enlace  <https://drive.google.com/file/d/0B9hHRpdvfs9KOWpZdTRiWW1yeFU/view?usp=sharing>`_.
   
       
Hoja Gastos Comunes (OB-4)
--------------------------

Para la selección de las historias de usuario el cuarto equipo se basó en MoSCow, metodología la cual consiste en clasificar las funcionalidades según su prioridad segun la siguiente escala:

.. csv-table:: Valoración MoSCow
   :header: "Valoración", "Detalle"
   :widths: 15,45

   Must Have (Imprescindibles),"Son funcionalidades que deben ser incluidas antes de que el producto pueda ser puesto en producción"
   Should Have (Importantes), "Son funcionalidades importantes y de gran valor para el usuario pero que no impiden poner el proyecto en marcha si no se tienen."
   Could Have (Buenas),"Son funcionalidades que sería deseable tener y podrían incluirse en caso de que hubiese recursos para ello"
   Wont Have (Excluidas), "Son funcionalidades que el cliente ha solicitado inicialmente pero que han sido  descartadas"

Luego de aplicar esta escala se determinó que las historias de usuario críticas y por tanto las que se deben implementar en esta primera iteración para el OB-4 son las siguientes:

.. csv-table:: Historias Criticas OB-4
   :header: "Código", "Como", "Quiero", "Para"
   :widths: 15, 10, 30,30
   
   HU-401,Arrendatario/Propietario,Conocer mis gastos comunes actuales e históricos, Reducir gastos y conocer si tengo sobre cobros o cobros erróneos
   HU-407,Administrador,Generar un gasto común (gastos adicionales o los gastos comunes ordinarios y las sumas destinadas a nuevas obras comunes),Efectuar cobros por cada gasto común.
   HU-403,Arrendatario/Propietario,Obtener mis facturas y mi estado de pago,Utilizarlos como prueba de la posesión de un bien o de un servicio


El detalle del trabajo realizado por este equipo se encuentra disponible en el siguiente `enlace  <https://drive.google.com/file/d/0B9hHRpdvfs9KSjhzTWstdFZPdUU/view?usp=sharing>`_.