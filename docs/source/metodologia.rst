Metodologia
===========

la metodología a utilizar será llamada "Visual Ad-hoc Methodology", la cual está inspirada en el trabajo que realizan los dibujantes de grandes compañías como Disney y Pixar al realizar sus películas o cortometrajes; pero aplicada al desarrollo de software. 

En su génesis está referido directamente al concepto de storyboard, donde se expone cada escena o frame de la película completa, acompañada de un dibujo, detalles y comentarios que ayuden a entender a cabalidad el contexto general y la linealidad de las acciones.

Esto, en conjunto con otras técnicas de educción y diseño conforman la metodología a utilizar.


Pasos
------------------

.. image:: metodologia/1.jpg


Stakeholders
^^^^^^^^^^^^
En este paso será definido el o los stakeholders asociados a nuestro caso de estudio, junto a los cuales se obtendrá la información pertinente para el resto de los pasos a seguir.

Para este proyecto el stakeholder seleccionado corresponde a una Administradora de edificios.


Vision, Objetivos y Áreas
^^^^^^^^^^^^^^^^^^^^^^^^^
Mediante alguna herramienta de educción se deberá detectar y segmentar la problemática y contexto en torno a estos tres elementos: Visión, Objetivos y Áreas.
Este paso permite entender y definir el dominio del problema, el alcance y las expectativas.


Historias de Usuario Por Áreas
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
En base a la información recopilada hasta este momento, se detallarán historias de usuario para cada una de las Áreas
en cuestión.

Selección de historias de usuario
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
De las historias de usuario detectadas, en base a su prioridad, se seleccionarán las más relevantes o críticas. Esta selección, también conocida como "Product Backlog" en la metodología Scrum, permitirá definir una primera versión del sistema a desarrollar.


Mockups
^^^^^^^
Utilizando alguna herramienta de modelado rápido de interfaces, se detallará gráficamente cada una de las pantallas con las que interactuará el usuario, las cuales más adelante serán implementadas.


Diseño del sistema
^^^^^^^^^^^^^^^^^^
En la etapa de diseño del sistema, se busca especificar las entidades relevantes para posteriormente desarrollar una implementación dirigida por modelos (MDD). El MDD necesita como insumo básicamente un diagrama entidad relación, el cual deberá estar en concordancia con las historias de usuario seleccionadas y el diseño de interfaz.

Implementación y Testing
^^^^^^^^^^^^^^^^^^^^^^^^
La implementación se refiere a la fase del proyecto en que se llevan a cabo las tareas necesarias para darle vida a la plataforma, las tecnologías a utilizar deberán ser seleccionadas de acuerdo a los requerimientos del proyecto y capacidades del equipo. EL testing por su parte hace referencia a las pruebas del sistema de manera de asegurar la calidad de este.


Deployment
^^^^^^^^^^
Corresponde a la fase en la cual el sistema se implanta para su uso final.
