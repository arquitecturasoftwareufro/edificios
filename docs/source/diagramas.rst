Diagramas de Clase y Casos de uso
=================================

Con los mockups ya estudiados, se procede a la elaboración de diagramas de clases y casos de uso que detallen la interfaz diseñada en los mockups, asegurandose así la coherencia del proyeto.  


Control de Turnos (OB-1)
------------------------

Para el control de turnos el equipo identifico las siguientes clases como las minimas necesarias para la implementación inicial del sistema:

- Persona
- Remplazante
- Usuario
- Administrador
- Funcionario
- Restricción
- Tipo de restricción
- Labor
- Turno

El detalle de las relaciones de las clases se puede apreciar en el siguiente diagrama
elaborado por el equipo:

.. image:: diagramas/clases-OB1.png

Este diagrama da origen al siguiente diagrama de casos de uso:

.. image:: diagramas/casosdeuso-OB1.png


Uso de Espacios Comunes (OB-2)
------------------------------

Por su parte el equipo de espacios comunes considera necesarias las siguientes clases para su sistema:

- Administrador
- Residente
- Departamento
- Arrendatario
- Propietario
- Reserva
- FormaPago
- Horario
- EspacioComun
- SalaReunion
- Quincho
- Inventario
- Articulo

El detalle del diagrama se encuentra disponible en la siguiente imagen:

.. image:: diagramas/clases-OB2.png

Resultando en el siguiente diagrama de casos de uso:

.. image:: diagramas/casosdeuso-OB2.png



Mantención de Equipamientos (OB-3)
----------------------------------

Para el equipo de mantenciones las clases mínimas necesarias son las siguientes:

- Mantención
- Calendario
- Encargado
- Edificio
- Conserje

Las cuales se relacionan como se indica en la siguiente imagen:

.. image:: diagramas/clases-OB3.png

Dando origen al siguiente diagrama de casos de uso:

.. image:: diagramas/casosdeuso-OB3.png


       
Hoja Gastos Comunes (OB-4)
--------------------------

El equipo de gastos comunes identifico las siguientes clases:

- Usuario
- Rol
- Cobro
- Proveedor

Relacionandose como se observa en el siguiente diagrama:

.. image:: diagramas/clases-OB4.png

Las cuales les permitieron elaborar el siguiente diagrama de casos de uso:

.. image:: diagramas/casosdeuso-OB4.png

Finalmente a esta altura, ya es posible identificar los requerimientos, los cuales han sido detallados en el siguiente `documento <https://drive.google.com/file/d/0B9hHRpdvfs9KS0lvR0dWVG5zTEU/view?usp=sharing>`_ .