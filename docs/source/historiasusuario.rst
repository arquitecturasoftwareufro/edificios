Historias de Usuario
====================

Las historias de usuario (o user story en inglés) describen una funcionalidad que, por sí misma, aporta valor al usuario. En este caso serán las interacciones del usuario con el sistema esperado.

Utilizando como referencia el mapa mental, se detectaron diversas historias de usuario las cuales se detallan a continuación. 

Para la gestión de las historias, se utilizó la herramienta Trello.

.. image:: userstories/1.png


Control de Turnos (OB-1)
^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Control de Turnos
   :header: "Código", "Como", "Quiero", "Para"
   :widths: 15, 10, 30,30

    HU-101,Conserje,Conocer los turnos designados,trabajar
    HU-102,Administrador,Asignar turnos,Establecer calendarios de trabajo
    HU-103,Arrendatario/Propietario,Conocer quien esta de turno,Solucionar mis posibles problemas
    HU-104,Administrador,Saber las horas totales trabajadas,No exceder lo dispuesto por la ley
    HU-105,Propietario,Conocer las atribuciones de los funcionarios,Que en caso de necesitar un servicio especial saber con quien acudir
    HU-106,Administrador,Conocer los turnos trabajador por cada funcionario,Definir salarios
    HU-107,Administrador,Conocer quien ha recibido cada pago de gastos comunes,Tener un control de posibles errores en el recibo de estos
    HU-108,Funcionario,Realizar una solicitud en caso de no poder asistir,Ser reemplazado sin dejar sin funcionario el lugar
    HU-109,Administrador,Obtener un listado de personas que trabajan como reemplazos,Administrar las solicitudes de reemplazo
    HU-110,Administrador,Obtener un informe de cada trabajador,Entregarlo a la directiva del condominio
    

Uso de Espacios Comunes (OB-2)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. csv-table:: Espacios Comunes
   :header: "Código", "Como", "Quiero", "Para"
   :widths: 15, 10, 30,30

    HU-201,Arrendatario/propietario,Registrar el uso de un espacio común,Reservarlo
    HU-202,Arrendatario/propietario,conocer la capacidad e inventario del espacio común,Saber si este cumple con las condiciones que requiero
    HU-203,Arrendatario/propietario,Saber la disponibilidad horaria de los espacios comunes,Saber si  este se ajusta a mi tiempo
    HU-204,Administrador,Saber que arrendatarios/propietarios han pagado por las reservas,Tener identificadas a las personas responsables del uso de espacios
    HU-205,Administrador,Saber el listado del inventario,Tener un control del inmobiliario
    HU-206,Administrador,Conocer el registro histórico de arriendos de espacios comunes,Saber de que manera varia el uso de estos a los largo del tiempo
    HU-207,Administrador,Modificar las reservaciones de espacios comunes,Reagendarlas o eliminarlas en caso de cualquier inconveniente
    HU-208,Arrendatario/propietario,Conocer el aspecto actual de los espacios comunes mediante fotografías,Tomarlo en cuanta al momento de decidir arrendar el lugar
    HU-209,Administrador,Saber los ingresos totales y subdividido en cada espacio por concepto de uso de espacio comunes,Realizar un análisis de rentabilidad
    HU-210,Arrendatario/propietario,Saber el costo asociado al arriendo de los espacios comunes,Tomar la decisión de arrendar o no 
    HU-211,Administrador,Tener una lista de contactos de cada uno de los técnicos o profesionales de mantenimiento y prestación de servicios,Tener acceso rápido a quienes puedan satisfacer las necesidades
    HU-212,Administrador,Registrar  o modificar espacios comunes,Que los residentes puedan arrendarlos

Mantención de Equipamientos (OB-3)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. csv-table:: Mantención Equipamiento
   :header: "Código", "Como", "Quiero", "Para"
   :widths: 15, 10, 30,30

    HU-301,Conserje,Conocer las fechas en las que corresponde realizar mantenimientos del distinto equipamiento de los edificios,Planificar estos acontecimientos
    HU-302,Administrador,Planificar los mantenimientos de los distintos equipos mes a mes,Establecer un calendario de mantenimientos
    HU-303,Administrador,Conocer los mantenimientos que se han realizado durante un mes,Compararlos con los mantenimientos programados con ese mes 
    HU-304,Administrador,revisar las mantenciones no realizadas,Reagendarlas
    HU-305,Conserje,Dar aviso de las mantenciones realizadas,Facilitar al administrador la planificación del calendario de mantenciones
    HU-306,Administrador,Poseer una agenda del personal de mantenimiento,Que así el conserje de turno tenga conocimiento del trabajador que llegará al edificio 
    HU-307,Conserje,Conocer los datos de contacto del personal de mantenimiento,informar de posibles eventualidades
    HU-308,Administrador,Que el conserje reciba la lista de mantenimientos que se ejecutarán durante su jornada de trabajo,Evitar cualquier problema con estos
    HU-309,Conserje,"Notificar al administrador de las tareas de mantenimiento no realizadas, junto con el motivo por el cual no fueron realizadas",Que el administrador tome las medidas correspondientes
    HU-310,Conserje,informar al administrador de mantenimientos no programados,Que este tome las medidas pertinentes
    
Hoja Gastos Comunes (OB-4)
^^^^^^^^^^^^^^^^^^^^^^^^^^
.. csv-table:: Hoja gastos comunes
   :header: "Código", "Como", "Quiero", "Para"
   :widths: 15, 10, 30,30
   
   HU-401,Arrendatario/Propietario,Conocer mis gastos comunes actuales e históricos, Reducir gastos y conocer si tengo sobre cobros o cobros erróneos
   HU-402,Arrendatario/Propietario,Obtener colilla de cobros individual,Saber el detalle con todos los costos asociados junto con el total a pagar
   HU-403,Arrendatario/Propietario,Obtener mis facturas y mi estado de pago,Utilizarlos como prueba de la posesión de un bien o de un servicio
   HU-404,Arrendatario/Propietario,Obtener un informe resumen de todos los cobros mensuales,Realizar un seguimiento y control de los resultados de las gestiones de cobro
   HU-405,Administrador,Crear una cuenta nueva asociada a un proveedor y a un tipo de gasto,Llevar un control sistemático y ordenado de los gastos del edificio
   HU-406,Administrador,Obtener un historial de todas las cuentas registradas,"Consultar,administrar o controlar todos los gastos registrados" 
   HU-407,Administrador,Generar un gasto común correspondiente a los gastos adicionales o los gastos comunes ordinarios y las sumas destinadas a nuevas obras comunes, Posteriormente efectuar cobros por cada gasto común
   HU-408,Arrendatario/Propietario,"Saber el detalle del fondo común de reserva derivado de los porcentajes de recargo sobre los gastos comunes, multas y uso de bienes comunes",Saber en que se utilizó dicho fondo
   HU-409,Arrendatario,Saber las fechas para los pago de gastos comunes,Saber hasta cuando realizar el pago
   HU-410,Arrendatario/Propietario,Conocer los pagos atrasados y sus detalles,"Saber si existe riesgo o hay multa, o castigo"
   HU-411,Propietario/Administrado,Saber los la suma total de cada tipo de gasto de cada mes de un año o periodo seleccionado,Un posterior uso en análisis y/o estimaciones 







