Unificando Modelos
==================

A estas alturas, los distintos equipos ya tienen suficiente conocimiento de la problemática y de aquello que se tiene que implementar por lo cual para definir estructuras estándares con las cuales trabajar se ha decidido unificar los modelos, definiendo así una plantilla para las vistas de los usuarios, y un modelo de la base de datos acorde a las funcionalidades esperadas.

Consideraciones
^^^^^^^^^^^^^^^

Luego de una revisión de los requerimientos, mockups, tiempo restante y capacidades del equipo se ha optado, en primer lugar por implementar el sistema dirigido hacia el administrador y funcionarios, dejando de lado al menos para esta primera implementación el acceso de los residentes. Por otro lado respecto a los mockup , el calendario considerado por el equipo cargo del OB-3 se ha descartado dado el tiempo que este requiere para ser correctamente implementado. 

Plantilla estándar en base a los mockup
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Luego de una revisión más precisa de los mockup, se pudo apreciar ciertos  elementos en común presentes en estos, para la mayoria de estos se consideró la utilización de una side-bar de navegación. Aquellos donde no se consideraron, se determinó que también podía ser implementada, sin perder funcionalidad e incluso mejorando la estructura de la vista. Así se determino que la estructura interna de la vista para los usuarios debería estar constituida como se aprecia en la siguiente imagen:

.. image:: /Unificacion/layout.png
    
Como se puede apreciar, esta compuesta básicamente por una side-bar y una top nav-bar la cual solo proporcionará acceso a la configuración de usuario. Se definen además secciones en las cuales cada equipo deberá implementar sus vistas.


Base de datos a partir de driagramas de clase
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Finalmente durante el análisis de las entidades propuestas se obtuvo el siguiente modelo, el cual responde a las necesidades planteadas.

.. image:: /Unificacion/bd.jpg