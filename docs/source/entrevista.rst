Entrevista
==========

La entrevista realizada al stakeholder, fue diseñada por el equipo de trabajo, haciendo uso de la técnica brainstorming, durante este proceso el equipo identificó distintos objetivos sobre los cuales se elaboró un modelo de entrevista el cual permitiera contextualizar al equipo con la situación actual, para establecer situaciones problemáticas, sobre las cuales se deseara implantar un sistema de software.

Las preguntas realizadas durante la entrevista son:

- ¿Cual es su cargo?
- ¿Cual es su función?
- ¿Cómo describiría la administración de los edificios?
- ¿Cuántas personas trabajan con usted en la administración de edificios?
- ¿Qué productos , servicios o documentos se entregan?
- ¿A quién se le entregan estos?
- ¿Qué proceso o parte del proceso se hace difícil en la administración de edificios?
- ¿Cómo le gustaría que fuera dicho proceso o que mejoraría de él?

Adicionalemente se definieron distintos flujos para llevar a cabo la entrevista a modo de preparación para las posibles eventualidades que pudieran ser encontradas durante el desarrollo de la entrevista.

El detalle de la elaboración y justificación de la entrevista puede ser encontrado `aquí  <https://drive.google.com/file/d/0B9hHRpdvfs9KSlZDLUNwQVhXaFk/view?usp=sharing>`_.

Un resumen de los datos obtenidos en la entrevista se puede observar en el siguiente `documento.  <https://drive.google.com/open?id=0B9hHRpdvfs9KanlJaVltMkEya2s>`_.
 
Durante el desarrollo de la entrevista se elaboró un mapa mental, el cual posteriormente fue refinado y es presentado en la siguiente sección, este contiene un mayor detalle de lo recopilado durante la entrevista.
