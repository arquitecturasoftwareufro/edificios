Selección de tecnologías
========================

Ahora que el alcance del proyecto, esta claramente definido, el equipo se enfrenta a la problemática de seleccionar las herramientas que le permitan llevar a cabo el proyecto dentro de los plazos establecidos y cumpliendo los requerimientos obtenidos. Con esto en mente se seleccionaron las siguientes herramientas:

Laravel
-------

.. image :: tecno/laravel.png

`Laravel <https://laravel.com>`_ es un framework, de código abierto diseñado para desarrollar aplicaciones y servicios web, basado en PHP 5 y 7. Laravel propone el patrón de desarrollo MVC, el cual se adapta de buena a manera al proyecto. Entre las caracteristicas importantes a recalcar de laravel encontramos:

- Posee un sistema de ruteo
- Utiliza blade como motor de plantillas
- Peticiones Fluent6
- Eloquent ORM
- Basado en Composer
- Soporte para el caché
- Soporte para MVC
- Usa componentes de Symfony
- Rápido aprendizaje

Por otro lado al ser un framework ampliamente utilizado y con una gran comunidad lo hace idoneo para la situación ya que esto contribuirá a rápida resolución de errores.

Laravel Generator
-----------------

.. image :: tecno/laravelgen.png

`Laravel Generator  <http://labs.infyom.com/laravelgenerator/>`_, es un paquete el cual facilita el proceso de instalación del framework, ayudando enormemente en la generación de los CRUD, migraciones ,controladores, integración de swagger ,así como la implementación de plantillas Front-END, la idea de este paquete, es simplificar la instalación del framework entregando un sistema configuración rápido, pero no por ello incompleto.

Swagger
-------

.. image :: tecno/swagger.jpeg

`Swagger  <http://swagger.io>`_ es un framework, el cual permite la descripción de API, lo cual permite la documentación automática del proyecto, de esta manera se obtiene una API, de fácil acceso para futuras iteraciones. La implementación de swagger en este caso se hizo a través del paquete `Swaggervel  <https://github.com/appointer/swaggervel>`_ , paquete disponible para el framework laravel y que hace uso de las tecnologias swagger.

Ejemplo de uso de las tecnologias
---------------------------------

.. raw:: html

    <div style="position: relative; padding-bottom: 5%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/YFwnlRedwbI" frameborder="0" allowfullscreen></iframe>
    </div>

Como se pudo apreciar, el uso de estas tecnologias en conjunto permite la implementación rápida de un sistema sencillo, obteniendose como resultado CRUD elaborados, dejando por delante solo trabajo para las funcionalidades específicas del proyecto. La aplicación de estas se puede observar en el siguiente `enlace  <http://projectoarquitectura.herokuapp.com>`_ .     