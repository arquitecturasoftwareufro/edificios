Mapa Mental
===========

Un mapa mental, es una método para extraer y organizar información de manera lógica y creativa, es útil para tomar notas, expresar ideas o resumir información. Para elaboración de este mapa mental se utilizó la información recopilada a través de la entrevista realizada al stakeholder se elaboró el siguiente `mapa mental <https://coggle.it/diagram/WRWsIiq99wABXUI_>`_, el cual detalla los temas tratados durante la entrevista.

En este mapa mental, se pude apreciar claramente que la información recopilada tiene relación con cuatro grandes areas:

Datos relacionados con su cargo
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Esta sección se asoció la información obtenida que guarda relación con el rol y funciones que realiza el stakeholder entrevistado en su institución , documentos utilizados en su trabajo diariom asi como también funcionarios que trabajan con ella y sus funciones.



Datos relacionados con las principales problemáticas existentes en la organización
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Esta tiene relación con los problemas que se desean abordar o minimizar con la implantación del sistema de software, en general corresponden a situaciones administrativas que pueden ser corregidas.


Datos del edificio
^^^^^^^^^^^^^^^^^^
Corresponde a los datos organizacionales de los edificios que administra el stakeholder, posee detalles importantes los cuales se deberán tener en cuenta en la elaboración del sistema.

Escenario ideal
^^^^^^^^^^^^^^^
La cuarta rama del mapa mental aborda el escenerio ideal del stakeholder, resulta de especial interes pues permite visualizar como el stakeholder desea que sea su organización a futuro, y permite al equipo responder de mejor manera al espectativas del stakeholder.
