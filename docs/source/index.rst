

Documentacion Proyecto Arquitectura de Software
===============================================

Documentación del proyecto semestral de la asignatura arquitectura de software, impartida por el Dr. Ricardo Gacitúa, este proyecto consiste en elaboración de un sistema de administración de edificios departamentales, haciendo uso de la metodología Visual Ad-hoc desarrollada para este proyecto.



.. toctree::
   :maxdepth: 2
   :caption: Indice:

   Descripcion Problematica <problematica>
   Metodologia <metodologia>
   Entrevista <entrevista>
   Mapa mental <mapamental>
   Historias de usuario <historiasusuario>
   Selección historias de usuario criticas <selhistoriasusuario>
   Diseño Mockups <disenomockups>
   Diagramas de clases y casos de uso <diagramas>
   Unificación de modelos <unificando>
   Selección de tecnologías <tecnologia>

