Diseño Mockups
==============

Parte fundamental de la metodología corresponde al diseño de los Mockups de la aplicación, estos darán origen a diagramas de clases, diagramas de casos de uso , además de ser una herramienta sumamente gráfica para definir el alcance en funcionalidades del sistema.


Control de Turnos (OB-1)
------------------------

.. image:: mockups/OB1.gif

Para cumplir los requerimientos de estas funcionalidades, se considera necesaria la funcionalidad de “login” de esta manera es posible la diferenciación de usuarios, en estos mockup se propone una vista compuesta por un panel de navegación lateral, el permita tanto a funcionarios como administradores navegar por el sistema para visualizar las distintas funcionalidades del mismo. Además considera en su parte central tablas, las cuales proporcionarán las funcionalidades requeridas. Como retroalimentación para el usuario se utilizan mensajes flotantes o emergentes los cuales entregarám información de las acciones realizadas. 

El detalle de los mockup se encuentra disponible en el siguiente `enlace  <https://drive.google.com/file/d/0B9hHRpdvfs9KMXdEMzNGTml6VTA/view?usp=sharing>`_.


Uso de Espacios Comunes (OB-2)
------------------------------

.. image:: mockups/OB2.gif

Al igual que para el OB-1, se propone un “login”, este debe reconocer además a usuarios residentes. Para los residentes se proponen cuatro botones, cada uno encargado de proporcionarle distintas funcionalidades al residente, entre estas se encuentran la reserva de espacios comunes, información de espacios comunes y hoja de gastos comunes. Dentro la opción reserva se visualiza un panel superior que permite seleccionar un espacio común, una ves seleccionado muestra información básica del espacio, adicionalmente se hace visible en la parte inferior el formulario que permite hacer registro de la reserva.

Por su parte para el administrador se propone una barra de navegación lateral que proporciona acceso a las distintas funcionalidades del sistema, se propone una vista de inicio, la cual debe entregar detalles de lo sucedido en el sistema. Desde la barra de navegación también se permite el acceso a espacios comunes, esta desplagrá un sub-barra con los espacios comunes creados además del acceso a la creación de espacios comunes. Dentro de la vista de creación se encuentra la opción para ingresar un nuevo espacio, especificando una fotografía del espacio. Además bajo esta opción encontramos tabulado un formulario para ingresar los detalles del espacio. Una ves ingresado los espacios comunes se hacen visibles en la sub-barra de navegación dentro de la vista de cada espacio se encuentran tabulada la información del espacio común agregando además información de reserva y disponibilidad.

Los Mockup del OB-2 se encuentran disponibles `aquí  <https://drive.google.com/file/d/0B9hHRpdvfs9KUlhOVWhLZ0I3ZGc/view?usp=sharing>`_ y `acá  <https://drive.google.com/file/d/0B9hHRpdvfs9KOHcwbzdLNjhhRlU/view?usp=sharing>`_.

Mantención de Equipamientos (OB-3)
----------------------------------

.. image:: mockups/OB3.gif

El equipo de mantenciones, asumió la presencia de un sistema de “login”, sin embargo este no se ve presente en los mockup. Se consideraron dos usuarios al funcionario conserje y al administrador. Proponiendo para ambos vistas similares compuestas por una barra lateral de navegación y un botón superior para el acceso a las configuraciones del usuario. Al administrador se le da acceso a una vista de resumen llamada calendario, en la cual puede visualizar el resumen de eventos sucedidos en el sistema, esta vista meramente informativa entrega información mediante el uso de mensajes emergentes, los cuales entregan el detalle del día. Por su parte la vista registro de mantenciones otorga información de las mantenciones realizadas ordenandolas por fechas y mostrando el estado de estas, a la izquierda de este detalle se encuentra un menú el cual tiene por objetivo el filtrado de mantenciones y dar acceso a la creación de mantenciones. El botón crear hace emergen un formulario con los detalles de la mantención a ser creada, es necesario añadir que un formulario similar se encuentra disponible presionado el icono “>” de las mantenciones este formulario permite la edición de la mantención, reagendación y eliminación de mantenciones. 

Por su parte al usuario conserje se le da acceso a un registro de mantenciones del dia, el similar al del administrador, el cual posee capacidad para editar ciertos campos de la mantención pudiendo de esta manera actualizar el estado de la mantecion y añadir comentarios a la misma. 

El detalle de los mockup se encuentra disponible en el siguiente `enlace  <https://drive.google.com/file/d/0B9hHRpdvfs9KcTVqdEVWYk8zbjg/view?usp=sharing>`_. 
       
Hoja Gastos Comunes (OB-4)
--------------------------

.. image:: mockups/OB4.gif

Finalmente el equipo de las hojas de gastos comunes, presento su mockups, en los cuales se puede visualizar la vista de "login", la cual tiene por funcionalidad discernir entre usuarios administradores y residentes. Una ves que se ha iniciado sesión en el sistema , para el administrador se propone una vista compuesta por una barra de navegación lateral, y una parte central en la que presenta una tabla , la cual proporciona las funcionalidades relacionadas al CRUD de hojas de gastos comunes.

Por su parte para el residente la sección central se compone por una tabla en la que puede visualizar el detalle de gastos comunes así como comprobantes de sus pagos.

Cabe destacar que para ambos usuarios se ha considerado necesario la inclusión de una vista de bienvenida al sistema.

El detalle de los mockups se encuentra disponible en el siguiente `enlace  <https://drive.google.com/file/d/0B9hHRpdvfs9KQWVOOFBfbDgwekU/view?usp=sharing>`_. 